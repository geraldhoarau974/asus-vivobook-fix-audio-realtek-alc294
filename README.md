Fix the audio issue for the Asus Vivobook pro 14 pro X with the Audio chipset Realtek ALC294

You will need to install this tool first:
sudo apt-get install alsa-tools

Then you need to download the file "audio.sh" and run the following command in the terminal and reboot the computer.

sudo bash ./audio.sh

In my case, It didn't not work after the first reboot, but it did work after the second reboot



The original code to fix the issue is not from me. I found it from the link bellow. I made the bash file to make it more simple for everyone. I hope it helps

the original code was taken from:
https://askubuntu.com/a/1394625/1628848
